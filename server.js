const express = require('express')
const app = express()

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

const users = []
let userId = 0

app.get('/', (req, res) => {
  res.render("index")
})

app.post("/", (req, res) => {
  users.push({ firstName: req.body.firstName, password: req.body.password })
  res.redirect(`/user/${users.length - 1}`)
  console.log(users);
})

app.param('id', (req, res, next, id) => {
  userId = id
  next();
})

app.route('/user/:id').get((req, res) => {
  res.render("login")
})

app.post("/user", (req, res) => {
  if (req.body.checkName == users[userId].firstName && req.body.checkPass == users[userId].password) {
    res.send("Logged in");
  } else {
    res.send("Error - inccorect user or password"); 
  }
})

app.listen(3000)